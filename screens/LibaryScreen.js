import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import LibraryStore from '../mobx/LibraryStore';
import {observer} from 'mobx-react';
import Loading from '../components/LoadingComponent';

const screenWidth = Dimensions.get('window').width;

@observer
class LibraryScreen extends Component {
  handleRemoveBook(id) {
    LibraryStore.removeBookInLibrary(id);
  }

  render() {
    const {navigation} = this.props;
    const {listBooks} = LibraryStore;
    console.log(JSON.stringify(listBooks));

    return (
      <View style={styles.all}>
        <Loading loading={LibraryStore.loading} />
        <View style={styles.categoryBook}>
          <Text style={[styles.textCategory]}>Trending</Text>
          <Text style={[styles.textCategory, styles.textCategoryActive]}>
            Categories
          </Text>
          <Text style={[styles.textCategory]}>Best seller</Text>
        </View>
        <ScrollView>
          <View style={styles.container}>
            <RenderListBook
              listBooks={listBooks}
              onRemoveBook={(id) => this.handleRemoveBook(id)}
            />
          </View>
        </ScrollView>
        <View style={styles.navBar}>
          <Button
            buttonStyle={[styles.buttonAction]}
            onPress={() => {
              navigation.navigate('Home');
            }}
            icon={
              <Icon color="#FC8B55" size={40} name="home" type="font-awesome" />
            }
          />
          <Button
            buttonStyle={[styles.buttonAction]}
            icon={
              <Icon color="#FC8B55" size={40} name="book" type="font-awesome" />
            }
          />
          <Button
            buttonStyle={[styles.buttonAction]}
            icon={
              <Icon
                color="#FC8B55"
                size={40}
                name="bookmark-o"
                type="font-awesome"
              />
            }
          />
          <Button
            buttonStyle={[styles.buttonAction]}
            icon={
              <Icon color="#FC8B55" size={40} name="user" type="font-awesome" />
            }
          />
        </View>
      </View>
    );
  }
}

const RenderBook = ({Book, onRemoveBook}) => {
  return (
    <View>
      {Book && (
        <View>
          <Image
            style={styles.imageBook}
            source={{
              uri: Book.image,
            }}
          />
          <View style={styles.parentBtnRemove}>
            <Button
              buttonStyle={[styles.btnRemoveBook]}
              onPress={() => {
                onRemoveBook(Book.id);
              }}
              icon={
                <Icon color="red" size={30} name="trash" type="font-awesome" />
              }
            />
          </View>
          <Text style={[styles.bookTitle]}>{Book.title}</Text>
        </View>
      )}
    </View>
  );
};

const RenderListBook = ({listBooks, onRemoveBook}) => {
  listBooks = listBooks.slice();
  const countRow = Math.round(listBooks.length / 2);
  let rowItems = new Array(countRow).fill(1);
  rowItems = rowItems.map((item) => {
    const item1 = listBooks.shift();
    const item2 = listBooks.shift();
    return {item1, item2};
  });
  console.log(countRow);

  return (
    <View>
      {rowItems.map((row) => {
        return (
          <View style={styles.rowItem}>
            <RenderBook
              Book={row.item1}
              onRemoveBook={(id) => onRemoveBook(id)}
            />
            <RenderBook
              Book={row.item2}
              onRemoveBook={(id) => onRemoveBook(id)}
            />
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    display: 'flex',
    flex: 1,
  },
  container: {
    paddingHorizontal: 10,
    flex: 1,
  },
  navBar: {
    position: 'absolute',
    height: 50,
    width: screenWidth,
    backgroundColor: 'white',
    bottom: 0,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buttonAction: {
    backgroundColor: 'white',
  },
  categoryBook: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 50,
    alignItems: 'center',
    marginHorizontal: 20,
  },
  text: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  textCategoryActive: {
    color: 'black',
  },
  textCategory: {
    color: 'grey',
    fontWeight: 'bold',
    fontSize: 20,
  },
  rowItem: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 15,
  },
  imageBook: {
    width: screenWidth / 2 - 30,
    height: screenWidth / 2,
    borderRadius: 20,
  },
  parentBtnRemove: {
    position: 'absolute',
    top: 3,
    right: 3,
  },
  btnRemoveBook: {
    width: 45,
    height: 45,
    borderRadius: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  bookTitle: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#dc5a2e',
  },
});

export default LibraryScreen;
