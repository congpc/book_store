import React, {Component} from 'react';
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import LibraryStore from '../mobx/LibraryStore';
import {observer} from 'mobx-react';
import Loading from '../components/LoadingComponent';

@observer
class DetailScreen extends Component {
  handleAddBookToLibrary(book) {
    LibraryStore.addBookToLibrary(book);
  }
  render() {
    const {navigation, route} = this.props;
    const {book} = route.params;

    return (
      <View style={styles.container}>
        <Loading loading={LibraryStore.loading} />
        <View style={styles.screen1}>
          <View style={styles.parentBntBack}>
            <Button
              buttonStyle={styles.btnBack}
              onPress={() => navigation.navigate('Home')}
              icon={
                <Icon
                  color="#FC8B55"
                  size={20}
                  name="arrow-left"
                  type="font-awesome"
                />
              }
            />
          </View>
          <Image
            style={styles.imageBookContent}
            source={{
              uri: book.image,
            }}
          />
        </View>
        <View style={styles.screen2}>
          <Text style={[styles.title]}>{book.title} </Text>
          <Text style={[styles.author]}>{book.author} </Text>
          <Text style={[styles.price]}>{book.price} </Text>
          <View style={styles.actionBook}>
            <Text style={[styles.optionBook, styles.optionBookActive]}>
              Description
            </Text>
            <Text style={[styles.optionBook]}>Review(20)</Text>
            <Text style={[styles.optionBook]}>Similliar</Text>
          </View>
          <View style={styles.viewDescription}>
            <ScrollView>
              <Text style={styles.description}>{book.description}</Text>
            </ScrollView>
          </View>
          <Button
            titleStyle={[styles.titleStyleButton, styles.titleBtnAdd]}
            buttonStyle={[styles.btnAdd]}
            title="Add to Library"
            onPress={() => {
              this.handleAddBookToLibrary(book);
            }}
            disabled={LibraryStore.checkBookExistOnLibrary(book.id)}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  screen1: {
    flex: 1,
    backgroundColor: '#ffd1b2',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  parentBntBack: {
    position: 'absolute',
    top: 20,
    left: 10,
  },
  btnBack: {
    height: 35,
    width: 35,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  imageBookContent: {
    width: 150,
    height: 210,
    borderRadius: 10,
  },
  screen2: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 0,
  },
  title: {color: 'black', fontWeight: 'bold', fontSize: 30},
  author: {color: 'grey', fontSize: 20, marginVertical: 5},
  price: {color: '#ffaaa5', fontWeight: 'bold', fontSize: 25},
  actionBook: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 30,
    alignItems: 'flex-end',
  },
  optionBookActive: {
    color: 'black',
  },
  optionBook: {
    color: 'grey',
    fontWeight: 'bold',
    fontSize: 16,
  },
  viewDescription: {
    marginTop: 10,
    height: 120,
  },
  description: {
    color: 'grey',
    fontSize: 15,
    lineHeight: 25,
  },
  btnAdd: {
    marginTop: 10,
    backgroundColor: '#ffaaa5',
  },
});

export default DetailScreen;
