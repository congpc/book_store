import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import {Input, Button, Icon} from 'react-native-elements';
import BookStore from '../mobx/BookStore';
import {observer} from 'mobx-react';
import Loading from '../components/LoadingComponent';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

@observer
class HomeScreen extends Component {
  componentDidMount() {
    BookStore.getListBook();
  }

  render() {
    const {navigation} = this.props;
    const {listBooks} = BookStore;
    console.log(BookStore.loading);

    return (
      <View style={styles.all}>
        <Loading loading={BookStore.loading} />
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.header}>
              <Text style={[styles.text, styles.txtUsername]}> Hi, Rizki</Text>
              <Text style={[styles.text]}> Discover Latest Book</Text>
              <Input
                inputContainerStyle={[styles.txtInput, styles.txtSearch]}
                inputStyle={styles.text}
                rightIcon={{
                  type: 'font-awesome',
                  name: 'search',
                  color: 'white',
                  containerStyle: styles.iconSearch,
                  size: 25,
                }}
              />
            </View>
            <View style={styles.body}>
              <View style={styles.categoryBook}>
                <Text style={[styles.textCategory, styles.textCategoryActive]}>
                  New
                </Text>
                <Text style={[styles.textCategory]}>Trending</Text>
                <Text style={[styles.textCategory]}>Best seller</Text>
              </View>
              <View style={styles.bookContent}>
                <Image
                  style={styles.imageBookContent}
                  source={{
                    uri:
                      'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/donna-tartt-book-quote-1531932390.jpg',
                  }}
                />
                <Image
                  style={styles.imageBookContent}
                  source={{
                    uri:
                      'https://www.penguin.com/wp-content/uploads/2020/04/BOL-Bookclub-PRHMobile.png',
                  }}
                />
              </View>
              <View style={styles.listBookContainer}>
                <Text style={[styles.textPopular]}>Popular</Text>
                <RenderBookItem listBooks={listBooks} navigation={navigation} />
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.navBar}>
          <Button
            buttonStyle={[styles.buttonAction]}
            icon={
              <Icon color="#FC8B55" size={40} name="home" type="font-awesome" />
            }
          />
          <Button
            buttonStyle={[styles.buttonAction]}
            onPress={() => {
              navigation.navigate('Library');
            }}
            icon={
              <Icon color="#FC8B55" size={40} name="book" type="font-awesome" />
            }
          />
          <Button
            buttonStyle={[styles.buttonAction]}
            icon={
              <Icon
                color="#FC8B55"
                size={40}
                name="bookmark-o"
                type="font-awesome"
              />
            }
          />
          <Button
            buttonStyle={[styles.buttonAction]}
            icon={
              <Icon color="#FC8B55" size={40} name="user" type="font-awesome" />
            }
          />
        </View>
      </View>
    );
  }
}

const RenderBookItem = ({listBooks, navigation}) => {
  return (
    <View style={styles.listBook}>
      {listBooks.map((book) => {
        return (
          <TouchableHighlight
            key={book.id}
            onPress={() => {
              navigation.navigate('Detail', {book});
            }}>
            <View style={styles.bookItem}>
              <Image
                style={styles.imageBookItem}
                source={{
                  uri: book.image,
                }}
              />
              <View style={styles.bookInformation}>
                <Text style={[styles.title]}>{book.title}</Text>
                <Text style={[styles.author]}>{book.author}</Text>
                <Text style={[styles.price]}>{book.price}</Text>
              </View>
            </View>
          </TouchableHighlight>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  all: {
    display: 'flex',
  },
  container: {
    paddingHorizontal: 10,
    flex: 1,
  },
  header: {
    height: 130,
    display: 'flex',
    justifyContent: 'space-around',
  },
  txtInput: {
    height: 45,
    borderColor: '#EFF0F1',
    borderWidth: 3,
    borderRadius: 10,
    paddingLeft: 10,
    backgroundColor: '#E4E2E2',
    marginHorizontal: -10,
  },
  categoryBook: {
    display: 'flex',
    flexDirection: 'row',
    width: 250,
    justifyContent: 'space-between',
  },
  text: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  textCategoryActive: {
    color: 'black',
  },
  textCategory: {
    color: 'grey',
    fontWeight: 'bold',
    fontSize: 16,
  },
  txtUsername: {
    fontSize: 15,
    marginBottom: -10,
    color: 'grey',
  },
  iconSearch: {
    backgroundColor: '#FFAAA5',
    width: 42,
    height: 41,
    display: 'flex',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: -6,
  },
  bookContent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  imageBookContent: {
    width: 190,
    height: 190,
    borderRadius: 10,
  },
  listBookContainer: {
    height: 1000,
    display: 'flex',
  },
  navBar: {
    position: 'absolute',
    height: 50,
    width: screenWidth,
    backgroundColor: 'white',
    bottom: 0,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buttonAction: {
    backgroundColor: 'white',
  },
  textPopular: {
    position: 'absolute',
    top: 0,
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
  },
  listBook: {
    position: 'absolute',
    top: 35,
    width: screenWidth - 20,
  },
  bookItem: {
    height: 90,
    marginBottom: 10,
    display: 'flex',
    paddingHorizontal: 10,
    paddingVertical: 3,
    flexDirection: 'row',
  },
  imageBookItem: {
    flex: 1,
    width: 70,
    height: 80,
    borderRadius: 7,
  },
  bookInformation: {
    flex: 4,
    display: 'flex',
    justifyContent: 'space-between',
    marginLeft: 10,
  },
  title: {color: 'black', fontWeight: 'bold', fontSize: 18},
  author: {color: 'grey', fontWeight: 'bold', fontSize: 16},
  price: {color: 'black', fontWeight: 'bold', fontSize: 16},
  imageLoading: {
    width: 60,
    height: 60,
    position: 'absolute',
    top: screenHeight / 2 - 30,
    left: screenWidth / 2 - 30,
  },
});

export default HomeScreen;
