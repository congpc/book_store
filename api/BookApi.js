import request from '../utils/request';

const getListBook = () => {
  return request({
    url: 'https://5ec39d77628c160016e706f3.mockapi.io/books',
    method: 'GET',
  }).then((data) => data);
};

const addBook = () => {
  return request({
    url: 'https://5ec39d77628c160016e706f3.mockapi.io/book',
    method: 'POST',
  }).then((data) => data);
};

const removeBook = (id) => {
  return request({
    url: 'https://5ec39d77628c160016e706f3.mockapi.io/book/',
    method: 'POST',
  }).then((data) => data);
};

export default {
  getListBook,
  addBook,
  removeBook,
};
