const request = async ({url, method = 'GET', body = {}, token = ''}) => {
  const config = {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: token,
    },
    body: body && JSON.stringify(body),
  };

  if (['GET', 'HEAD'].includes(method.toUpperCase())) {
    delete config.body;
  }

  return fetch(url, config)
    .then((res) => {
      try {
        return res.json();
      } catch (e) {
        throw e;
      }
    })
    .then((data) => {
      return data;
    })
    .catch((error) => {
      throw error;
    });
};

export default request;
