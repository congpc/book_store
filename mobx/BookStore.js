import {types, flow} from 'mobx-state-tree';
import bookApi from '../api/BookApi';

const Book = types.model({
  id: types.string,
  title: types.string,
  image: types.string,
  description: types.string,
  price: types.number,
  author: types.string,
});

const BookStore = types
  .model('bookStore', {
    loading: false,
    listBooks: types.array(Book),
  })
  .views((self) => {
    return {};
  })
  .actions((self) => {
    const getListBook = flow(function* () {
      self.loading = true;
      try {
        const data = yield bookApi.getListBook();
        self.listBooks = data;
        self.loading = false;
      } catch (error) {
        console.error('Failed to fetch projects', error);
        self.loading = false;
      }
      return self.listBooks;
    });
    return {
      getListBook,
    };
  });

export default BookStore.create({});
