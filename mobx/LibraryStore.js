import {types, flow} from 'mobx-state-tree';
import bookApi from '../api/BookApi';

const Book = types.model({
  id: types.string,
  title: types.string,
  image: types.string,
  description: types.string,
  price: types.number,
  author: types.string,
});

const LibraryStore = types
  .model('libraryStore', {
    listBooks: types.array(Book),
    loading: false,
  })
  .views((self) => {
    return {
      checkBookExistOnLibrary(id) {
        const check = self.listBooks.find((book) => book.id === id);
        return check && check.id === id ? true : false;
      },
    };
  })
  .actions((self) => {
    const addBookToLibrary = flow(function* (book) {
      self.loading = true;
      try {
        const response = yield bookApi.addBook();
        console.log(response);
        if (response.success) {
          console.log('goi api them thanh cong');
          self.listBooks.push({...book});
        }
        self.loading = false;
      } catch (error) {
        console.error('Failed to fetch projects', error);
        self.loading = false;
      }
      console.log('data sau khi them', JSON.stringify(self.listBooks));
      return self.listBooks;
    });

    const removeBookInLibrary = flow(function* (id) {
      self.loading = true;
      try {
        const response = yield bookApi.removeBook(id);
        console.log('ket qua api xoa', response);
        if (response.success) {
          console.log('goi api xoa thanh cong voi id', id);
          console.log('data truoc khi xoa', JSON.stringify(self.listBooks));
          self.listBooks = self.listBooks.filter((book) => book.id !== id);
          console.log('data sau khi xoa', JSON.stringify(self.listBooks));
        }
        self.loading = false;
      } catch (error) {
        console.error('Failed to fetch projects', error);
        self.loading = false;
      }
      return self.listBooks;
    });
    return {
      addBookToLibrary,
      removeBookInLibrary,
    };
  });

export default LibraryStore.create({});
